docker: backend frontend ki
	cd docker && $(MAKE)
SUBDIRS=backend frontend ki

$(SUBDIRS):
	$(MAKE) -C $@
clean:
	cd docker && $(MAKE) clean
	cd backend && $(MAKE) clean
.PHONY: clean docker $(SUBDIRS)
