package de.icross;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@RegisterRestClient
@Path("/")
public interface KIRestClient {

    @POST
    @Path("process/{name}")
    @Produces("")
    Object process(@PathParam("name") String filename);
}
