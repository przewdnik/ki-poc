package de.icross;

import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSFile;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.*;

@Path("predict")
@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
public class KipocResource {

    @Inject
    MongoDbFactory mongoFactory;

    @Inject
    @RestClient
    KIRestClient kiRestClient;


    @GET
    @Path("result")
    public Response getLastResult()
    {
        BasicDBObject obj = new BasicDBObject();
        return findOneResult(obj);
    }

    private Response findOneResult(BasicDBObject obj) {
        Document doc = findOne(obj);
        if (doc == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        final ArrayList res =  Optional.ofNullable((ArrayList) doc.get("result")).orElse(new ArrayList());
        return Response.ok(res).build();
    }

    @GET
    @Path("resultFiles/{filename}")
    public Response getResultFileList(@PathParam("filename") String file) {
        BasicDBObject obj = new BasicDBObject();
        obj.put("filename", file);
        Document doc = findOne(obj);
        if (doc == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        final ArrayList res =  Optional.ofNullable((ArrayList) doc.get("resimgs")).orElse(new ArrayList());
        return Response.ok(res).build();
    }

    @GET
    @Path("fps/{filename}")
    public Response getFPS(@PathParam("filename") String file) {
        BasicDBObject obj = new BasicDBObject();
        if (!"null".equals(file)) {
            obj.put("filename", file);
        }
        Document doc = findOne(obj);
        if (doc == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        final Integer res =  Optional.ofNullable((Integer) doc.get("fps")).orElse(24);
        return Response.ok(res).build();
    }

    @GET
    @Path("resultFiles")
    public Response getResultLastFileList() {
        BasicDBObject obj = new BasicDBObject();
        Document doc = findOne(obj);
        if (doc == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        final ArrayList res =  Optional.ofNullable((ArrayList) doc.get("resimgs")).orElse(new ArrayList());
        return Response.ok(res).build();
    }


    private Document findOne(BasicDBObject obj) {
        MongoDatabase db = mongoFactory.getDb();
        BasicDBObject sort = new BasicDBObject();
        sort.put("_id", -1);
        Iterator it = db.getCollection("results").find(obj).sort(sort).iterator();
        if (it.hasNext()) {
            return (Document) it.next();
        }
        return null;
    }

    @GET
    @Path("result/{filename}")
    public Response getResult(@PathParam("filename") String file) {
        BasicDBObject obj = new BasicDBObject();
        obj.put("filename", file);
        return findOneResult(obj);

    }

    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response upload(@FormDataParam("file") InputStream uploadedInputStream,
                           @FormDataParam("file") FormDataContentDisposition fileDetail) {
        DB db = mongoFactory.getDB();
        GridFS fs = new GridFS(db/*, "uploads"*/);
        GridFSFile f =  fs.createFile(uploadedInputStream, fileDetail.getFileName());
        f.save();
        ObjectId id = (ObjectId) f.getId();
        f = fs.find(id);
        kiRestClient.process(f.getFilename());
        return Response.ok(f.getId()).build();
    }

    @GET
    @Path("files")
    public Response getFiles()
    {
        DB db = mongoFactory.getDB();
        List res = db.getCollection("results").distinct("filename");
        return Response.ok(res).build();
    }

    @GET
    @Path("resultFile/{filename}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getResultFile(@PathParam("filename") String file) {
        DB db = mongoFactory.getDB();
        GridFS fs = new GridFS(db, "results");
        InputStream f = fs.findOne(file).getInputStream();
        CacheControl cc = new CacheControl();
        cc.setNoCache(false);
        cc.setMaxAge(1000 * 60 * 24 * 1);
        cc.setMustRevalidate(false);
        return Response.ok(f, MediaType.APPLICATION_OCTET_STREAM).cacheControl(cc)
                .build();
    }
}
