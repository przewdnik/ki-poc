package de.icross.model;

import java.util.List;

public class Result {
    private String media;
    private String object;
    private List<Sequence> sequences;

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public Result(String media, String object, List<Sequence> sequences) {
        this.media = media;
        this.object = object;
        this.sequences = sequences;
    }
}
