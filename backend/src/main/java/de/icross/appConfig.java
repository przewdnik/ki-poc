package de.icross;

import org.glassfish.jersey.media.multipart.MultiPartFeature;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class appConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> resources = new HashSet();
        resources.add(MultiPartFeature.class);
        resources.add(CORSFilter.class);
        resources.add(KipocResource.class);
        return resources;
    }

}
