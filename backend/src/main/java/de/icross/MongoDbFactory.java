package de.icross;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;

import javax.inject.Singleton;

@Singleton
public class MongoDbFactory {

    private MongoDatabase db;

    private DB _db;

    public MongoDbFactory() {
        ServerAddress addr = new ServerAddress("nosql");
        //  MongoClient mongo = new MongoClient(addr, Collections.singletonList(cred));
        MongoClient mongo = new MongoClient(addr);
        _db = mongo.getDB("kipoc");
        db = mongo.getDatabase("kipoc");

    }

    public MongoDatabase getDb() {
        return db;
    }

    public DB getDB() {
        return _db;
    }

}
