import math

def center(bbox):
        return [(bbox[2]+bbox[0])/2, (bbox[3]+bbox[1])/2]

def dist(a, b):
        return math.hypot(a[0]-b[0], a[1]-b[1])

def close(abbox, bbbox):
        return dist(center(abbox), center(bbbox))<100

def log(app, msg):
        if (app):
                app.logger.warning(msg)
        else:
                print(msg)

def processObject(app, oname, obbox, results, i, dups):
        if oname in results:
            fms = results[oname]["frames"]
            # what was in last frame
            lastFrame = list(filter(lambda el: el[2]==i-1, fms))
            print(lastFrame)
            other = list(filter(lambda el: el[2]!=i-1, fms))
            newFrame = []
            found = False
            if lastFrame:
                for obj in lastFrame:
                        (lbbox, f, _) = obj
                        if (close(obbox, lbbox)):
                                log(app, oname+" is close enough to last frame, dist="+ str(dist(center(obbox), center(lbbox))))
                                # it still it this frame
                                if (dups or not found):
                                        newFrame.append([obbox, f, i])
                                if (not dups):
                                        found = True;
                        else:
                                log(app, oname+" is *NOT* close enough to last frame, dist="+ str(dist(center(obbox), center(lbbox)))+" last bbox: "+str(lbbox)+" new bbox: "+str(obbox))
                                # no longer in this frame
                                newFrame.append([lbbox, f, i-1])
            else:
                got_something = False
                if (not dups):
                        # duplicates are not ok, lets see
                        thisFrame = list(filter(lambda el: el[2]==i, other))
                        log(app, "frame until now: "+str(thisFrame))
                        for obj in thisFrame:
                                (cbbox, f, _ ) = obj
                                if (close(cbbox, obbox)):
                                        log(app, oname+" is close enough to another in the same frame, dist="+ str(dist(center(obbox), center(cbbox))))
                                        got_something = True;
                                        break;
                if (got_something):
                        log(app, oname+" is duplicate, bbox="+str(obbox))
                else:        
                        log(app, oname+" is new, bbox="+str(obbox))
                        newFrame.append([obbox, i,i])
            sort_o = lambda d: d[1];
            other.sort(key = sort_o)
            newFrame.sort(key = sort_o)
            
            # recreate the list
            results[oname]["frames"]=other
            results[oname]["frames"].extend(newFrame)
        else:
            # not in results
            results[oname]= {"frames": [[obbox,i,i]]}
        return results
