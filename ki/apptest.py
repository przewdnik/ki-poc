import unittest
from v_process import processObject, close

class TestProcessImage(unittest.TestCase):

        def test_empty(self):
                self.assertEqual({"bottle": {"frames": [[[0,0,10,10], 1, 1 ]]}},processObject(None, "bottle", [0, 0, 10, 10], {}, 1, True))

        def test_add(self):
                prev = {"bottle": {"frames": [[[0,0,10,10], 1, 1 ]]}};
                self.assertEqual({"bottle": {"frames": [[[0,0,10,10], 1, 2 ]]}}, processObject(None, "bottle", [0, 0, 10, 10], prev, 2, True))

        
        def test_dup(self):
                prev = {"bottle": {"frames": [[[0,0,10,10], 1, 2]]}};
                self.assertEqual({"bottle": {"frames": [[[0,0,10,10], 1, 2 ]]}}, processObject(None, "bottle", [0, 0, 10, 10], prev, 2, False))

        def test_close(self):
                self.assertEqual(close([1180, 61, 1357, 582], [1186, 89, 1339, 348]), False);

        def test_rl(self):
                prev = {'bottle': {'frames': [[[1180, 61, 1357, 582], 22, 23], [[750, 36, 932, 635], 1, 23], [[1186, 89, 1339, 348], 23, 23]]}};
                new = [1180, 61, 1357, 582];
                res = {'bottle': {'frames': [
                                [[750, 36, 932, 635], 1, 23], 
                                [[1180, 61, 1357, 582], 22, 24],
                                [[1186, 89, 1339, 348], 23, 23],
                                ]}};
                self.assertEqual(res, processObject(None, "bottle", new, prev, 24, False));

        def test_rl_dups(self):
                prev = {'bottle': {'frames': [[[1180, 61, 1357, 582], 22, 23], [[750, 36, 932, 635], 1, 23], [[1186, 89, 1339, 348], 23, 23]]}};
                new = [1180, 61, 1357, 582];
                res = {'bottle': {'frames': [
                                [[750, 36, 932, 635], 1, 23], 
                                [[1180, 61, 1357, 582], 22, 24],
                                [[1186, 89, 1339, 348], 23, 23],
                                ]}};
                self.assertEqual(res, processObject(None, "bottle", new, prev, 24, True));


        def test_dups_same_frame(self):
                prev = {};
                new = [0, 0, 10, 10];
                res = {"bottle": {"frames": [[[0,0,10,10], 1, 1 ]]}};
                self.assertEqual(res, processObject(None, "bottle", new, prev, 1, False));
                self.assertEqual(res, processObject(None, "bottle", new, prev, 1, False));

        def test_sort(self):
                prev = {"bottle": {"frames": [[[0,0,10,10], 9, 10 ], [[0,0,15,15], 1, 10]]}};
                new = [0, 0, 10, 10];
                res = {"bottle": {"frames": [[[0,0,15,15], 1, 10 ], [[0,0,10,10], 9, 10]]}};
                self.assertEqual(res, processObject(None, "bottle", new, prev, 10, False));
                
if __name__ == '__main__':
    unittest.main()
