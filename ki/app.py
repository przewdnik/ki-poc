#!/usr/bin/python
import skvideo
skvideo.setFFmpegPath("/tmp/ffmpeg/");
import skvideo.io

from flask import Flask, jsonify, request
from flask_restful import Resource, Api

from luminoth import Detector, read_image, vis_objects

from pymongo import MongoClient
import gridfs

import os
from threading import *
from queue import *

import logging
from logging.handlers import RotatingFileHandler

import numpy as np


from v_process import processObject

detname = os.getenv('DETECTOR', "accurate")
num_worker_threads = int(os.getenv('NUM_WORKER_THREADS', 1))
snapshots_from_movie = int(os.getenv('SNAPSHOTS_FROM_MOVIE', 4))
snapshots_from_movie = snapshots_from_movie - 1;
duplicates_ok = os.getenv("DUPLICATES_OK", "true")
dups = duplicates_ok.lower() in ["true", "yes"]

chunk_size=20000

app = Flask(__name__)
api = Api(app)

cl = MongoClient("nosql", 27017)
db = cl.get_database("kipoc")
gridFS = gridfs.GridFS(db)
resultsgridFS = gridfs.GridFSBucket(db, bucket_name='results')
results_dir = "/tmp/results/"
#detector = Detector("fast")
detector = Detector(detname)

if not os.path.exists(results_dir):
    os.makedirs(results_dir)

def prune(filename):
        for f in resultsgridFS.find({"filename": filename}):
                resultsgridFS.delete(f._id)

def predImage(fname):
        app.logger.warning("Predicting image");
        image = read_image("/tmp/"+fname)
        app.logger.warning("Predicting "+fname)
        objects = detector.predict(image)
        app.logger.warning("Predicted "+fname+": "+str(objects))
        dbObj = {"filename": fname, "result": objects, "resimgs": [fname]}
        dbid = db.results.insert_one(dbObj).inserted_id
        app.logger.warning("Inserted results to db, id="+str(dbid))
        vis_objects(image, objects).save(results_dir+fname)
        app.logger.warning("Created result file for "+fname)
        f = open(results_dir+fname, "rb")        
        prune(fname)
        resultsgridFS.upload_from_stream(fname, f)
        f.close()



def predVideo(fname):
        app.logger.warning("Predicting video");
        f = "/tmp/"+fname;
        r = "/tmp/results/"+fname[:-4]+".mp4";
        outputFiles = [];
        writer = skvideo.io.FFmpegWriter(r)
        probe = skvideo.io.ffprobe(f);
        app.logger.warning(probe)
        frames_num = int(probe['video']['@nb_frames'])
        fps = round(eval(probe['video']['@r_frame_rate']))
        app.logger.warning("Frames num: "+str(frames_num)+ " fps: "+str(fps))
        i = 0;
        results = {}
        output_frames = 0;
        for frame in skvideo.io.vreader(f):
            i+=1
            app.logger.warning("Frame "+str(i)+" of "+str(frames_num));
            objects = detector.predict(frame)
            if ((frames_num/snapshots_from_movie)*output_frames <= i):
                o_name=fname[:-4]+"_"+str(output_frames)+".jpg"
                app.logger.warning("Saving result frame "+str(output_frames)+" to "+o_name);
                vis_objects(frame, objects).save(results_dir+o_name) # we should add our tracking stuff, but for now...
                outputFiles.append(o_name)
                of = open(results_dir+o_name, "rb")
                prune(o_name)
                resultsgridFS.upload_from_stream(o_name, of)
                of.close()
                output_frames = output_frames + 1
            app.logger.warning("Predicted: "+str(objects))
            for o in objects:
                oname = o["label"]
                obbox = o["bbox"]
                results = processObject(app, oname, obbox, results, i, dups)
            app.logger.warning("results: "+ str(results))
            image = vis_objects(frame, objects)
            writer.writeFrame(np.array(image))
        app.logger.warning("Predicted "+fname+": "+str(results))
        reslist = []
        for k,v in results.items():
            reslist.append({"label": k, "frames": v["frames"]})
        dbObj = {"filename": fname, "fps": fps, "result": reslist, "resimgs": outputFiles}
        dbid = db.results.insert_one(dbObj).inserted_id
        writer.close()



def workTask(fname, f):
        lf = open("/tmp/"+fname, "wb")
        while (lf.write(f.read(chunk_size))>0):
            continue;
        lf.close()
        f.close()
        ext = fname.split('.')[-1].lower()
        if (ext in ["mov", "avi"]):
            predVideo(fname)
        else:
            predImage(fname);
    

# worker thread for processing
def worker():
    while True:
        (fname, f) = queue.get()
        workTask(fname, f)
        queue.task_done()

queue = Queue()

for i in range(num_worker_threads):
     t = Thread(target=worker)
     t.daemon = True
     t.start()

class ProcessResource(Resource):
    def post(self, name):
        f = gridFS.find_one({"filename":name})
        if f is None:
            return 'No such image in db', 404
        app.logger.warning("Adding "+name+" to processing queue")
        queue.put_nowait( (name, f) )  # async
        #workTask(name, f)  # syncronous
        return '', 201
    

api.add_resource(ProcessResource, '/process/<name>')


if __name__ == '__main__':
    handler = RotatingFileHandler('flask.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.DEBUG)
    app.logger.addHandler(handler)
    app.logger.warning("Starting ki server, detector="+detname+" workerThreads="+str(num_worker_threads)+" duplicates allowed: "+str(dups))
    app.run(host='0.0.0.0', debug=True)
