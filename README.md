Short notes

1. In order to build and run type:
make
in toplevel directory
2. Results (both for video and image, but video results are not working right now):
  docker cp ki:/tmp/results .
creates a results directory on host
3. Parameters in docker/compose/docker-compose.yaml:
  * DETECTOR: name of detector used in recognition ("fast" or "accurate")
  * NUM_WORKER_THREADS: how many jobs are handled at once in ki
  * DUPLICATES_OK: if tracking code should consider same elements in similiar place the same object (good to use when we don't trust detection) 
  * SNAPSHOTS_FROM_MOVIE: how many frames from movie recognition are saved to show in GUI
4. Logs from detection:
  docker logs ki
For example in order to see what arguments were used:
  docker logs ki 2>&1 | grep Starting
