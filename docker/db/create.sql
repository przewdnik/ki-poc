CREATE TABLE users (
 id serial UNIQUE PRIMARY KEY,
 name VARCHAR,
 password varchar
);

CREATE TABLE inputs (
 id serial UNIQUE PRIMARY KEY,
filename VARCHAR,
user_id INTEGER REFERENCES users(id)
);
