#!/bin/sh

PARTS="frontend backend db ki nosql"

for part in $PARTS;
	do
		echo Building image $part
		cd $part
		docker build . --tag=kipoc-$part
		cd ..
	done


docker-compose -f compose/docker-compose.yaml up
