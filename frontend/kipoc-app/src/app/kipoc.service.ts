import { Injectable } from '@angular/core';
import { Result } from './result';

import { Observable, of } from 'rxjs';
import { HttpClient, HttpRequest, HttpHeaders, HttpEvent, HttpEventType } from '@angular/common/http';
import { map } from  'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class KipocService {

private url = 'http://localhost:8080/kipoc/predict/';

constructor(private http: HttpClient) { }

getFiles(): Observable<string[]> {
	return this.http.get<string[]>(this.url + 'files');
}

getResults(name): Observable<Result[]> {
    return this.http.get<Result[]>(this.url + 'result/'+name);
}
getFPS(name): Observable<number> {
    return this.http.get<number>(this.url + 'fps/'+name);
}

getLastResults(): Observable<Result[]> {
    return this.http.get<Result[]>(this.url + 'result');
}

getResultUrls(urls: string[]) {
        return urls.map(u => this.url+'resultFile/'+u);
}

getResultUrl(url: string) {
        return this.url+'resultFile/'+url;
}

getResultFiles(name): Observable<string[]> {
        var u = this.url+'resultFiles';
        if (name) {
                u += "/" + name;
        }
        return this.http.get<string[]>(u);
}

upload(data) {
        console.log('uploading ${data.name}');
        const call =  new HttpRequest('POST', this.url + 'upload', data,

            {  reportProgress: true,
            // observe: 'events'
            }
            );
        return this.http.request(call).pipe(
                            map(event => this.getEventMessage(event, data.get('file')))
            );
}



        /** Return distinct message for sent, upload progress, & response events */
private getEventMessage(event: HttpEvent<any>, file: File) {
  switch (event.type) {
    case HttpEventType.Sent:
    // return `Uploading file "${file.name}" of size ${file.size}.`;
    return null;

    case HttpEventType.UploadProgress:
      // Compute and show the % done:
      const percentDone = Math.round(100 * event.loaded / event.total);
      return `${file.name}: ${percentDone}%`;

    case HttpEventType.Response:
    return null; // `File "${file.name}" was completely uploaded!`;

    default:
    // return `File "${file.name}" surprising upload event: ${event.type}.`;
    return null;
  }
}

}
