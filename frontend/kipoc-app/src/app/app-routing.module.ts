import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImageUploadComponent } from './image-upload/image-upload.component'
import { OutputComponent }  from './output/output.component'
import { FilelistComponent } from './filelist/filelist.component'

const r: Routes = [
        { path: "upload", component: ImageUploadComponent },
        { path: "filelist", component: FilelistComponent }, 
        { path: "outputLast", component: OutputComponent },
        { path: "output/:name", component: OutputComponent },
        
]

@NgModule({
  declarations: [],
  imports: [ RouterModule.forRoot(r) ],
  exports: [
        RouterModule
  ]
})
export class AppRoutingModule { }
