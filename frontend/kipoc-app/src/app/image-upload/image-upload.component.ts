import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from  '@angular/forms';

import { KipocService } from '../kipoc.service';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {
form: FormGroup;
public uploadResponse: string;
  public error: string;

  constructor(private formBuilder: FormBuilder, private service: KipocService) { }

  ngOnInit() {
  this.form = this.formBuilder.group({
      uploadFile: ['']
    });
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('uploadFile').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.form.get('uploadFile').value);
    if (this.form.get('uploadFile').value) {

    this.service.upload(formData).subscribe(
    res =>  {if (res) { this.uploadResponse = res; }}
    );
}
  }

}
