import { Pipe, PipeTransform } from '@angular/core';



@Pipe({name: 'timestamp'})
export class TimestampPipe implements PipeTransform {
  transform(n: number, fps: number): string {
    // 1:05:05
    // min = 
    n -= 1;
    let min = Math.floor(n / (60*fps))
    n -= min * 60 * fps;
    let ret = String(min);
    ret += ":";
    let sec = Math.floor(n / fps);
    n -= sec * fps;
    ret += String(sec).padStart(2, '0');
    ret += ":"
    ret += String(Math.floor(1000*n /fps)).padStart(3, '0');
    return ret;
  }
}
