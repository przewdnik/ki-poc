export class Result {
    bbox: number[];
    label: string;
    prob: number;
}
