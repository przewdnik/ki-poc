import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { KipocService } from '../kipoc.service';
import { fresult} from './fresult';

@Component({
  selector: 'app-filelist',
  templateUrl: './filelist.component.html',
  styleUrls: ['./filelist.component.css']
})

export class FilelistComponent implements OnInit {
  files: fresult[];

  constructor(private service: KipocService) { };

  mapFiles(fls: string[]) {
        this.files = [];
        for (let id in fls) {
                let f = fls[id];
                this.service.getResultFiles(f).subscribe(fls => this.files[id]={file: f, img: this.service.getResultUrl(fls[0])});
        }
  }

  ngOnInit() {
	this.service.getFiles().subscribe(f => this.mapFiles(f));
  }

}
