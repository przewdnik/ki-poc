import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ImageUploadComponent } from './image-upload/image-upload.component';
import { OutputComponent } from './output/output.component';
import { FilelistComponent } from './filelist/filelist.component';
import { AppRoutingModule } from './app-routing.module';
import { TimestampPipe } from './timestamp.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ImageUploadComponent,
    OutputComponent,
    FilelistComponent,
    TimestampPipe
  ],
  imports: [
  BrowserModule,
  HttpClientModule,
  ReactiveFormsModule,
  AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
