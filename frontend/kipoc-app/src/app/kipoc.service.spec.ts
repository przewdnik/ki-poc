import { TestBed } from '@angular/core/testing';

import { KipocService } from './kipoc.service';

describe('KipocService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KipocService = TestBed.get(KipocService);
    expect(service).toBeTruthy();
  });
});
