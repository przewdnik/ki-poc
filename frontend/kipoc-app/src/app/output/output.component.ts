import { Component, OnInit } from '@angular/core';
import { Result } from '../result';
import { KipocService } from '../kipoc.service';

import { Observable, interval } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {

res: Result[];
pictures: string[];
fps: number;

getResults(): void {
        const name = this.route.snapshot.paramMap.get('name');
        //console.log(name);
        var o: Observable<Result[]>;
        if (name) {
                o = this.service.getResults(name);
                } else 
                {
                o = this.service.getLastResults();
                }
        o.subscribe(r => this.res = r);
        this.service.getResultFiles(name).subscribe(p => this.pictures = this.service.getResultUrls(p));
        this.service.getFPS(name).subscribe(f => this.fps = f);
//	const timer = interval(4 * 1000);
//	const call = flatMap(() => this.service.getResults());
//	call(timer).subscribe(r => this.res = r );
}


  constructor(private service: KipocService,
              private route: ActivatedRoute) { }

  ngOnInit() {
   
   this.getResults();
  }

}
